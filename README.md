# Alice Ding_s Course Material

计算机英语-课程资源库

## Description

本课程的适用对象为计算机科学类专业（Computer Science）的大学本科一年级学生，本课程是一门计算机导论类的基础课程，教授计算机科学的基础技术和知识。
课程目标为：使同学们对计算机科学中的术语、理论、应用和技术有基本的了解，同时具备能够自学进阶类课程的能力。
上课时间：周一 3、4节课
上课教室：C404
上课平台：钉钉

## Getting Started

### Content

* [Data Storage](https://gitlab.com/Alice0924/alice-ding_s-course-material/-/blob/main/material/slides/CS01.pdf)
* [Data Manipulation](https://gitlab.com/Alice0924/alice-ding_s-course-material/-/blob/main/material/slides/CS02.pdf)
* [Operating Systems](https://gitlab.com/Alice0924/alice-ding_s-course-material/-/blob/main/material/slides/CS03.pdf)
* [Networking and the Internet](https://gitlab.com/Alice0924/alice-ding_s-course-material/-/blob/main/material/slides/CS04.pdf)
* [Algorithms](https://gitlab.com/Alice0924/alice-ding_s-course-material/-/blob/main/material/slides/CS05.pdf)
* [Programming Languages](https://gitlab.com/Alice0924/alice-ding_s-course-material/-/blob/main/material/slides/CS06.pdf)
* [Data Abstractions](https://gitlab.com/Alice0924/alice-ding_s-course-material/-/blob/main/material/slides/CS07.pdf)
* [Database Systems](https://gitlab.com/Alice0924/alice-ding_s-course-material/-/blob/main/material/slides/CS08.pdf)
* [Artificial Intelligence](https://gitlab.com/Alice0924/alice-ding_s-course-material/-/blob/main/material/slides/CS09.pdf)

### Score(成绩评定)

* 1、形成性成绩（40%）：考勤（10%）+课程作业成绩（30%）
* 2、期末试卷成绩（60%）

## Authors

Contributors names and contact info

- Name: Alice Ding  丁利华
- E-mail: aliceding19900924@gmail.com
- Phone: 18931312002

## License

This project is licensed under the [Alice Ding] License - see the LICENSE.md file for details

## Recommendations For Reading

Inspiration, code snippets, etc.
* [Computer Science: An Overview](https://gitlab.com/Alice0924/alice-ding_s-course-material/-/blob/main/material/book/Glenn_Brookshear-Computer_Science_An_Overview-EN.pdf)
* [计算机科学概论](https://gitlab.com/Alice0924/alice-ding_s-course-material/-/blob/main/material/book/682476%20%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%A7%91%E5%AD%A6%E6%A6%82%E8%AE%BA(%E7%AC%AC12%E7%89%88).pdf)
* [词汇](http://www.heycode.com/book/art/13638.html)   2 units/week
* [剑桥字典](https://dictionary.cambridge.org/us/dictionary/english) 

